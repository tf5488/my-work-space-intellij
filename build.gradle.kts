// Gradle 插件列表
plugins {
    id("java")
    id("org.jetbrains.intellij") version "1.9.0"
}

// 插件信息
group = "com.test.group"
version = "1.0-SNAPSHOT"

repositories {
    // maven 仓库地址
    mavenCentral()
}

// Configure Gradle IntelliJ Plugin 配置 Gradle intellij 依赖
// 阅读更多文档: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    // 编译的 IDEA 版本，使用版本编号，可以用大版本，也可以指定特定的小版本
    // 如果是大版本，插件校验的时候会把大版本所对应的所有的小版本进行逐一验证
    // 如果是本地验证，将会下载所有需要验证的 IDEA 版本进行验证<比较耗时>
    version.set("2021.3")
    // 支持的 IDEA 平台类型，IC：社区版；IU：企业版
    type.set("IC")

    // 依赖的插件列表
    plugins.set(listOf(/* Plugin Dependencies */))
}

tasks {
    // Set the JVM compatibility versions
    // jvm 兼容性版本，后面是版本号
    withType<JavaCompile> {
        sourceCompatibility = "11"
        targetCompatibility = "11"
        // 兼容中文字符
        options.encoding = "UTF-8"
    }

    // 插件兼容的 IDEA 版本范围
    patchPluginXml {
        // 起始
        sinceBuild.set("213")
        // 结束
        untilBuild.set("223.*")
    }

    // 插件签名(可以忽略)
    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    // 自动发布插件任务(可以忽略)
    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }
}
