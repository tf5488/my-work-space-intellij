# my-work-space-intellij

#### 介绍
intellij IDEA 插件
简化 IDEA 工作空间的配置流程，允许使用云端配置选项，提供一些简单的辅助工具。

#### 功能介绍
1. 项目文件结构打印
   <img alt="img_2.png" height="500" src="img_2.png" width="500"/>
2. 项目文件结构支持选定base目录
3. 项目文件结构支持筛选配置（配置忽略项）(支持使用.gitignore文件过滤)

#### 项目启动
gradle -> tasks -> intellij -> runlde

<img alt="img.png" height="500" src="img.png" width="400"/>

#### 注意事项
1. 项目使用 gradle,需要将 gradle 的配置 JDK 设置成 11.


#### 参考资料
项目基于插件 demo 开发，demo地址：https://gitee.com/tf5488/intellij-plugin-demo