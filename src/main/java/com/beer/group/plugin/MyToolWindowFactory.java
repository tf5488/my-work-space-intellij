package com.beer.group.plugin;

import com.beer.group.plugin.fileTree.panel.ConsolePanel;
import com.beer.group.plugin.fileTree.panel.FileTreePanel;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.*;
import com.intellij.ui.content.ContentManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Collections;

/**
 * 自己工具窗口的工厂类
 * Nothing can be injected as it runs in the root pico container.
 *
 * @author tianfeng
 */
public class MyToolWindowFactory implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(Project project, final ToolWindow toolWindow) {
        var contentManager = toolWindow.getContentManager();

        addFileTreeTab(project, contentManager);

        toolWindow.setType(ToolWindowType.DOCKED, null);
    }

    private static void addFileTreeTab(Project project, @NotNull ContentManager contentManager) {
        FileTreePanel fileTreePanel = new FileTreePanel(project);
        var currentFileContent = contentManager.getFactory()
                .createContent(
                        fileTreePanel,
                        "项目结构",
                        false);
        // 是否可以关闭
        currentFileContent.setCloseable(false);
        contentManager.addContent(currentFileContent);
    }

}
