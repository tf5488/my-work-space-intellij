package com.beer.group.plugin.fileTree.action;

import java.util.List;

/**
 * @author tianfeng
 */
public class FileTreeNode {

    /**
     * 名称
     */
    private String name;

    /**
     * 子目录列表
     */
    private List<FileTreeNode> childrenList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FileTreeNode> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<FileTreeNode> childrenList) {
        this.childrenList = childrenList;
    }
}
