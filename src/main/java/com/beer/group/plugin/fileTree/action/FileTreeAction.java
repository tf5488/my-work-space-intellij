package com.beer.group.plugin.fileTree.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 项目结构
 *
 * @author tianfeng
 */
public class FileTreeAction extends AnAction {


    @Override
    public void actionPerformed(AnActionEvent e) {
        Project project = e.getProject();

        PrintlnUtil.printlnNormal(project, "开始解析项目结构......");
        assert project != null;
        printFileTree(project);
        PrintlnUtil.printlnNormal(project, "解析项目结构完成");
    }

    private void printFileTree(Project project) {
        // 获取项目根目录
        String projectFilePath = project.getBasePath();
        PrintlnUtil.printlnNormal(project, "当前项目根目录地址:" + projectFilePath);

        // 加载 .gitignore 配置文件
        PrintlnUtil.printlnNormal(project, "加载配置文件:" + projectFilePath + "/.gitignore");
        FileTreeFilter.loadGitFilter(projectFilePath + "/.gitignore");

        List<FileTreeNode> treeList = fileList(projectFilePath);
        handle(project, treeList, "");
    }


    /**
     * 打印处理器，支持使用过滤器
     */
    private void handle(Project project, List<FileTreeNode> treeList, String prefix) {
        for (int i = 0; i < treeList.size(); i++) {
            // 打印当前节点
            FileTreeNode fileTreeNode = treeList.get(i);

            // 执行过滤器规则
            List<String> gitFilter = FileTreeFilter.getGitFilter();
            if (gitFilter.contains(fileTreeNode.getName())) {
                continue;
            }

            String a = "└";
            String pre = prefix + a;
            if (treeList.size() > 1 && treeList.size() != i + 1) {
                String b = "├";
                pre = prefix + b;
            }
            String c = "─";
            PrintlnUtil.printlnNormal(project, pre + c + fileTreeNode.getName());
            // 打印子节点
            if (!fileTreeNode.getChildrenList().isEmpty()) {
                String d = "│";
                String e = "  ";
                handle(project, fileTreeNode.getChildrenList(), prefix + d + e);
            }
        }
    }

    // 获取子节点
    public static List<FileTreeNode> fileList(String path) {
        List<FileTreeNode> list = new ArrayList<>();
        File f = new File(path);
        File[] fs = f.listFiles();
        for (File fil : fs) {
            // todo fil 文件中可以直接获取下级目录，不需要重新创建对象
            FileTreeNode fileTreeNode = new FileTreeNode();
            fileTreeNode.setName(fil.getName());

            List<FileTreeNode> fileList = new ArrayList<>();
            if (fil.isDirectory()) {
                System.out.println("direct:" + fil.getName());
                fileList = fileList(fil.getAbsolutePath());
                System.out.println("direct:" + fil.getName() + ",exist files:" + fileList.size());
            }

            fileTreeNode.setChildrenList(fileList);
            list.add(fileTreeNode);
        }
        return list;
    }


}
