package com.beer.group.plugin.fileTree.action;

import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.project.Project;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 打印简单工具类
 *
 * @author lk
 * @version 1.0
 * @date 2020/8/23 17:14
 */
public class PrintlnUtil {

    /**
     * 多项目控制台独立性
     */
    public static Map<Project, ConsoleView> consoleViewMap = new ConcurrentHashMap<>(16);

    public static void printsInit(ConsoleView consoleView) {
        consoleView.print(" ============================================================================ " + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("    MyWorkSpace" + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("    A Personal WorkSpace Framework :)" + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("    https://gitee.com/tf5488/my-work-space-intellij" + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("\n", ConsoleViewContentType.NORMAL_OUTPUT);
        consoleView.print("  ============================================================================ " + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
    }

    /**
     * 输出语句
     *
     * @param project                项目
     * @param rowLine                行数据
     * @param consoleViewContentType 输出颜色
     */
    public static void println(Project project, String rowLine, ConsoleViewContentType consoleViewContentType) {
        ConsoleView consoleView = consoleViewMap.get(project);
        if (consoleView != null) {
            consoleView.print(rowLine, consoleViewContentType);
        }
    }

    /**
     * 输出 NORMAL_OUTPUT 语句,自动换行
     */
    public static void printlnNormal(Project project, String text) {
        ConsoleView consoleView = consoleViewMap.get(project);
        if (consoleView != null) {
            consoleView.print(text + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
        }
    }

    public static void setConsoleView(Project project, ConsoleView consoleView) {
        consoleViewMap.put(project, consoleView);
    }

    public static ConsoleView getConsoleView(Project project) {
        return consoleViewMap.get(project);
    }

}
