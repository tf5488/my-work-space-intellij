package com.beer.group.plugin.fileTree.panel;

import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.tools.SimpleActionGroup;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Collection;

/**
 * 简单的工具窗口画板
 *
 * @author tianfeng
 */
public abstract class AbstractIssuesPanel extends SimpleToolWindowPanel {
    private static final String ID = "MyToolWindow";
    protected final Project project;
    private ActionToolbar mainToolbar;

    protected AbstractIssuesPanel(Project project) {
        super(false, true);
        this.project = project;
    }

    protected void setToolbar(Collection<AnAction> actions) {
        // 构建一个工具栏,工具栏会包含多个 action 菜单
        if (mainToolbar != null) {
            mainToolbar.setTargetComponent(null);
            super.setToolbar(null);
            mainToolbar = null;
        }
        mainToolbar = ActionManager.getInstance().createActionToolbar(ID, createActionGroup(actions), false);
        mainToolbar.setTargetComponent(this);
        var toolBarBox = Box.createHorizontalBox();
        toolBarBox.add(mainToolbar.getComponent());
        super.setToolbar(toolBarBox);
        mainToolbar.getComponent().setVisible(true);
    }

    private static ActionGroup createActionGroup(Collection<AnAction> actions) {
        var actionGroup = new SimpleActionGroup();
        actions.forEach(actionGroup::add);
        return actionGroup;
    }

}
