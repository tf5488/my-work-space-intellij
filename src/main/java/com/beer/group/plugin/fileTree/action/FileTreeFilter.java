package com.beer.group.plugin.fileTree.action;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 过滤器模块
 */
public class FileTreeFilter {

    final static List<String> gitFilter = new ArrayList<>();

    public static void loadGitFilter(String path) {
        File file = new File(path);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String s = null;
            while ((s = reader.readLine()) != null) {
                if (!s.contains("#") && s.trim().length() > 0) {
                    s = s.replace("/", "");
                    gitFilter.add(s);
                    System.out.println(s);
                }
            }
            reader.close();
        } catch (IOException ioException) {
            System.out.print("文件读取失败");
        }

    }

    public static List<String> getGitFilter() {
        return gitFilter;
    }
}
