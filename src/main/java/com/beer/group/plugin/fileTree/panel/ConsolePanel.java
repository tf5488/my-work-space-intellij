package com.beer.group.plugin.fileTree.panel;

import com.beer.group.plugin.fileTree.action.PrintlnUtil;
import com.intellij.execution.filters.Filter;
import com.intellij.execution.filters.TextConsoleBuilder;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * console 窗口
 *
 * @author lk
 * @version 1.0
 * @date 2020/4/10 22:00
 */
public class ConsolePanel {

    /**
     * 创建 输出面板
     *
     * @param project 项目
     * @return
     */
    public ConsoleView createConsole(@NotNull Project project, List<Filter> filterList) {
        // 构建一个输出窗口
        TextConsoleBuilder consoleBuilder = TextConsoleBuilderFactory.getInstance().createBuilder(project);
        consoleBuilder.filters(filterList);
        ConsoleView console = consoleBuilder.getConsole();
        PrintlnUtil.setConsoleView(project, console);
        PrintlnUtil.printsInit(console);
        return console;
    }


    public JComponent createConsolePanel(ConsoleView view) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(view.getComponent(), BorderLayout.CENTER);
        return panel;
    }

}
