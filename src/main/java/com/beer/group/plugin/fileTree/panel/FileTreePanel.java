/*
 * SonarLint for IntelliJ IDEA
 * Copyright (C) 2015-2023 SonarSource
 * sonarlint@sonarsource.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02
 */
package com.beer.group.plugin.fileTree.panel;

import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.project.Project;

import javax.swing.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author tianfeng
 */
public class FileTreePanel extends AbstractIssuesPanel implements Disposable {

    public FileTreePanel(Project project) {
        super(project);

        // 配置工具 table
        setToolbar(actions());

        // 配置内容窗口
        ConsolePanel consolePanel = new ConsolePanel();
        final ConsoleView consoleView = consolePanel.createConsole(project, Collections.emptyList());
        final JComponent panelConsolePanel = consolePanel.createConsolePanel(consoleView);
        super.setContent(panelConsolePanel);

    }

    @Override
    public void dispose() {
        // Nothing to do
    }

    private static Collection<AnAction> actions() {
        return List.of(
                // 可以获取 plugin.xml 文件中的 actionID 注入到窗口的侧边
                ActionManager.getInstance().getAction("FileTreeAction")
        );
    }

}
